<?
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

AddEventHandler("main", "OnBuildGlobalMenu", "MyOnBuildGlobalMenu");
function MyOnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
{
    $aModuleMenu[] = Array(
        "parent_menu" => "global_menu_services",
        "icon"        => "default_menu_icon",
        "page_icon"   => "default_page_icon",
        "text"        => "Импорт пользователей",
        "title"       => "Импорт пользователей",
        "url"         => "user_import_run.php",
        "items"       => ""
    );
}