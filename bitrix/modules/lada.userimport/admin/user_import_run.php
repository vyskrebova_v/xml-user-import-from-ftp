<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/lada.userimport/classes/general/importUsersFromXML.php");
//var_dump(file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/lada.userimport/classes/general/importUsersFromXML.php"));

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle("Импорт пользователей");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

if(CModule::IncludeModule("lada.userimport")) {
    $import = new importUsersFromXML();
    $import->getUserListXML();
}



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");