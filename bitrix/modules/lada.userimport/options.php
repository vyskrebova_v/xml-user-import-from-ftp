<?
use Bitrix\Main\Localization\Loc;

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
if (!class_exists('CMainPage')){
    include_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/mainpage.php");
}
Loc::loadMessages(__FILE__);
$module_id = "lada.userimport";
$siteID = \CMainPage::GetSiteByHost();

CModule::IncludeModule($module_id);

global $APPLICATION;

$aTabs = array(
    array(
        "DIV"     => "index",
        "TAB"     => Loc::getMessage("TAB_GENERAL_SETTINGS"),
        "ICON"    => "lada.userimport_settings",
        "TITLE"   => Loc::getMessage("TAB_GENERAL_SETTINGS_TITLE"),
        "OPTIONS" => array(
            "include_ftp_connect" => array(
                Loc::getMessage("OPT_USE_FTP_CONNECTION"),
                array("checkbox", "N")
            ),
            "ftp_host_name" => array(
                Loc::getMessage("OPT_HOST"),
                array("text")
            ),
            "ftp_login" => array(
                Loc::getMessage("OPT_LOGIN_LABEL"),
                array("text")
            ),
            "ftp_password" => array(
                Loc::getMessage("OPT_PASS_LABEL"),
                array("text")
            ),
            "ftp_port" => array(
                Loc::getMessage("OPT_PORT_LABEL"),
                array("text")
            ),
            "path_file" => array(
                Loc::getMessage("OPT_PATH_FILE_LABEL"),
                array("text")
            )
        )
    )
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);
if ($REQUEST_METHOD == "POST" && strlen($Update.$Apply.$RestoreDefaults) > 0 && check_bitrix_sessid()){
    if (strlen($RestoreDefaults) > 0)
        COption::RemoveOption($module_id, '', $siteID);
    else{
        foreach ($aTabs as $i => $aTab){
            foreach ($aTab["OPTIONS"] as $name => $arOption){
                $disabled = array_key_exists("disabled", $arOption) ? $arOption["disabled"] : "";
                if ($disabled)
                    continue;

                $val = $_POST[$name];
                if ($arOption[1][0] == "checkbox" && $val != "Y")
                    $val = "N";

                COption::SetOptionString($module_id, $name, $val, $arOption[0], $siteID);
            }
        }
    }

    ob_start();
    $Update = $Update.$Apply;
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
    ob_end_clean();
}
$tabControl->Begin();
?>
<form method="post"
      action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?=urlencode($mid)?>&amp;lang=<?=LANGUAGE_ID?>"
      id="options">
    <?
    foreach ($aTabs as $caTab => $aTab){
        $tabControl->BeginNextTab();
        if ($aTab["DIV"] != "rights" && $aTab["DIV"] != "variables"){ // �� ����� �������
            foreach ($aTab["OPTIONS"] as $name => $arOption){
                /*if ($bVarsFromForm)
                    $val = $_POST[$name];
                else*/
                    $val = COption::GetOptionString($module_id, $name, '', $siteID);
                $type     = $arOption[1];
                $disabled = array_key_exists("disabled", $arOption) ? $arOption["disabled"] : "";
                ?>
                <tr <? if (isset($arOption[2]) && strlen($arOption[2]))
                    echo 'style="display:none" class="show-for-'.htmlspecialcharsbx($arOption[2]).'"' ?>>
                    <td width="40%" <? if ($type[0] == "textarea")
                        echo 'class="adm-detail-valign-top"' ?>>
                        <label for="<? echo htmlspecialcharsbx($name) ?>"><? echo $arOption[0] ?>:</label>
                    <td width="30%">
                        <? if ($type[0] == "checkbox"){
                            ?>
                            <input type="checkbox"
                                   name="<? echo htmlspecialcharsbx($name) ?>"
                                   id="<? echo htmlspecialcharsbx($name) ?>"
                                   value="Y"<? if ($val == "Y")
                                echo " checked"; ?><? if ($disabled)
                                echo ' disabled="disabled"'; ?>><? if ($disabled)
                                echo '<br>'.$disabled; ?><?
                        } elseif ($type[0] == "text"){
                            ?>
                            <input type="text"
                                   size="<? echo $type[1] ?>"
                                   maxlength="255"
                                   value="<? echo htmlspecialcharsbx($val) ?>"
                                   name="<? echo htmlspecialcharsbx($name) ?>">
                            <?
                        } elseif ($type[0] == "textarea"){
                            ?>
                            <textarea rows="<? echo $type[1] ?>"
                                      name="<? echo htmlspecialcharsbx($name) ?>"
                                      style="width:100%"><? echo htmlspecialcharsbx($val) ?></textarea>
                            <?
                        } elseif ($type[0] == "select"){
                            ?><? if (count($type[1])){
                                ?>
                                <select name="<? echo htmlspecialcharsbx($name) ?>"
                                        onchange="doShowAndHide()">
                                    <? foreach ($type[1] as $key => $value){
                                        ?>
                                        <option value="<? echo htmlspecialcharsbx($key) ?>" <? if ($val == $key)
                                            echo 'selected="selected"' ?>><? echo htmlspecialcharsEx($value) ?></option>
                                        <?
                                    } ?>
                                </select>
                                <?
                            } else{
                                ?><? echo "Ошибка! Нет созданных элементов"; ?><?
                            } ?><?
                        } elseif ($type[0] == "note"){
                            ?><? echo BeginNote(), $type[1], EndNote(); ?><?
                        } ?>
                    </td>
                    <td width="30%">
                        <? if ($arOption[3]){
                            ?>
                            <p><? echo $arOption[3]; ?></p>
                            <?
                        } ?>
                    </td>
                </tr>
                <?
            }
        } elseif ($aTab["DIV"] == "variables"){
            ?>
            <tr>
                <th><? echo GetMessage("CODE_VIEW"); ?></th>
                <th align="center"><? echo GetMessage("PHP_VIEW"); ?></th>
                <th><? echo GetMessage("DESCRIPTION_VIEW"); ?></th>
            </tr>
            <? foreach ($arVars as $c => $varArr){
                ?>
                <tr>
                    <td>
                        <a href="javascript:void(0)"
                           class="copy_to_clipboard"><?=$varArr["CODE"];?></a>
                    </td>
                    <td align="center"><?=$varArr["PHP"];?></td>
                    <td><?=$varArr["DESC"];?></td>
                </tr>
                <?
            } ?><?
        } elseif ($aTab["DIV"] == "rights"){ // ������������ �� ������ ����, ������ ��� � �������� �� ����� ����������� � ��������� �����
            require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
        }
    } ?>

    <? $tabControl->Buttons(); ?>
    <input type="submit"
           name="Update"
           value="<?=GetMessage("MAIN_SAVE")?>"
           title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>"
           class="adm-btn-save">
    <?=bitrix_sessid_post();?>
    <? $tabControl->End(); ?>
</form>
<script>
    function doShowAndHide(){
        var form    = BX('options');
        var selects = BX.findChildren(form, {tag: 'select'}, true);
        for(var i = 0; i < selects.length; i++){
            var selectedValue = selects[i].value;
            var trs           = BX.findChildren(form, {tag: 'tr'}, true);
            for(var j = 0; j < trs.length; j++){
                if (/show-for-/.test(trs[j].className)){
                    if (trs[j].className.indexOf(selectedValue) >= 0)
                        trs[j].style.display = 'table-row';
                    else
                        trs[j].style.display = 'none';
                }
            }
        }
    }

    BX.ready(doShowAndHide);

    // ����������� ������
    var copyEmailBtns = document.querySelectorAll(".copy_to_clipboard");
    for(var i = 0; i < copyEmailBtns.length; i++){
        copyEmailBtns[i].addEventListener("click", function(e){
            // ������� ������
            // var textEl = copyEmailBtn.parentElement.nextElementSibling;
            var range = document.createRange();
            range.selectNode(this);
            window.getSelection().addRange(range);

            try {
                // ������, ����� �� ������� �����, �������� ������� ����������
                var successful = document.execCommand("copy");
                if (successful){
                    console.log("Copy email command was successful");
                }
            }catch(err) {
                console.log("Oops, unable to copy");
            }

            window.getSelection().removeAllRanges();
        });
    }
</script>
