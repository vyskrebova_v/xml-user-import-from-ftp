<?

use Bitrix\Main\Localization;

Localization\Loc::loadMessages(__FILE__);

class importUsersFromXML
{
    static $MODULE_ID = "lada.userimport";

    var $FTP_HOST_NAME;
    var $FTP_LOGIN;
    var $FTP_PASS;
    var $FTP_PORT;
    var $PATH_FILE;
    var $SITE_ID;
    var $ftpConnect;
    var $ERROR_MESSAGE;

    public function __construct()
    {
        if (!class_exists('CMainPage'))
            include_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/mainpage.php");

        $this->SITE_ID = \CMainPage::GetSiteByHost();
        $this->FTP_HOST_NAME = COption::GetOptionString(self::$MODULE_ID, "ftp_host_name", '', $this->SITE_ID);
        $this->FTP_LOGIN = COption::GetOptionString(self::$MODULE_ID, 'ftp_login', '', $this->SITE_ID);
        $this->FTP_PASS = COption::GetOptionString(self::$MODULE_ID, 'ftp_password', '', $this->SITE_ID);
        $this->FTP_PORT = COption::GetOptionString(self::$MODULE_ID, 'ftp_port', '', $this->SITE_ID);
        $this->PATH_FILE = COption::GetOptionString(self::$MODULE_ID, 'path_file', '', $this->SITE_ID);

        if(!empty($this->FTP_HOST_NAME)) {
            if(!empty($this->FTP_PORT)) {
                $this->ftpConnect = ftp_connect($this->FTP_HOST_NAME, $this->FTP_PORT);
            } else {
                $this->ftpConnect = ftp_connect($this->FTP_HOST_NAME);
            }
        }
        if(!$this->ftpConnect){
            $this->ERROR_MESSAGE[] = 'не удалось установить соединение';
        }

    }

    /**
     * @return string
     */
    public function getUserListXML() {
        $pathLocalFile = $_SERVER['DOCUMENT_ROOT'].'/upload'. $this->PATH_FILE;
        ftp_pasv($this->ftpConnect, true);
        $resultFtpLogin = ftp_login($this->ftpConnect, $this->FTP_LOGIN, $this->FTP_PASS);
        if(!$resultFtpLogin)
            $this->ERROR_MESSAGE[] = 'Ошибка соединения';
        else {
            $isFileReceived = null;
            if(file_exists($pathLocalFile)) {
                if(!$isSameFile = filesize($pathLocalFile) == ftp_size($this->ftpConnect, $this->PATH_FILE)) {
                    $isFileReceived = DownloadFile($pathLocalFile);
                }
            } else {
                $isFileReceived = DownloadFile($pathLocalFile);
            }
            if($isSameFile or $isFileReceived) {
                $xmlContent = new SimpleXMLElement(file_get_contents($pathLocalFile));
                //$arxml = json_decode(json_encode($xmlContent), true);
                //\Bitrix\Main\Diag\Debug::dump($arxml['employee'][0]);

                /*foreach ($arxml['employee'] as $itemEmployee) {
                    //\Bitrix\Main\Diag\Debug::dump($itemEmployee["@attributes"]);
                    foreach ($itemEmployee as $nameFiled => $field){
                        $arEmployee[$itemEmployee['@attributes']['uid']][$nameFiled] = $field;
                    }
                }*/
                \Bitrix\Main\Diag\Debug::dump($xmlContent->employee[0]);

                foreach ($xmlContent->employee as $itemEmployee) {
                    //\Bitrix\Main\Diag\Debug::dump($itemEmployee->attributes()->uid);
                    $uid = json_decode(json_encode($itemEmployee->attributes()->uid), true)[0];
                    //\Bitrix\Main\Diag\Debug::dump($uid);
                    foreach ($itemEmployee as $nameFiled => $field){
                        //\Bitrix\Main\Diag\Debug::dump($field->attributes());
                        $arEmployee[$uid][$nameFiled] = json_decode(json_encode($field), true)[0];
                        $attr = $field->attributes();
                        if(!empty($attr)) {
                            $attr = json_decode(json_encode($attr), true)["@attributes"];
                            $arEmployee[$uid][$nameFiled]['attributes'] = $attr ;

                            //\Bitrix\Main\Diag\Debug::dump($arEmployee[$uid][$nameFiled]);
                        }
                    }
                }

                \Bitrix\Main\Diag\Debug::dump($arEmployee);
            }
        }
    }

    /**
     * @return bool
     */
    private function DownloadFile ($pathLocalFile) {
        $isFileReceived = ftp_get($this->ftpConnect, $pathLocalFile, $this->PATH_FILE, FTP_BINARY);
        if(!$isFileReceived)
            $this->ERROR_MESSAGE[] = 'Не удалось скачать файл ' . $this->PATH_FILE;
        else
            return $isFileReceived;

    }

    /**
     * @return mixed
     */
    public function getERRORMESSAGE()
    {
        return $this->ERROR_MESSAGE;
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        ftp_close($this->ftpConnect);
    }


}